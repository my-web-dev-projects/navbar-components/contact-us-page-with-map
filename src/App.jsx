import { useState } from 'react'
import './App.css'
import mapDemo from "./assets/map-demo.png"

// importing SVGs downloaded from https://remixicon.com/
import addressIcon from "./assets/map-pin-2-fill.svg"
import mailIcon from "./assets/mail-fill.svg"
import phoneIcon from "./assets/phone-fill.svg"

import linkedInIcon from "./assets/facebook-circle-fill.svg"
import facebookIcon from "./assets/linkedin-box-fill.svg"
import instagramIcon from "./assets/instagram-fill.svg"
import threadsIcon from "./assets/threads-fill.svg"
import twitterIcon from "./assets/twitter-x-fill.svg"
import { OpenMap } from './components/openMap/OpenMap'

function App() {
  const contactInfos = [{
    id: 1,
    icon: addressIcon,
    text: "Opp. to Kirti College, Kashinath Dhuru Road, Off. Veer Savarkar Marg, Chandrakant Dhuru Wadi, Dadar(W), Mumbai, Maharashtra",
  },
  {
    id: 2,
    icon: mailIcon,
    text: "Email@panda.com",
  },
  {
    id: 3,
    icon: phoneIcon,
    text: "+91-1234567890",
  },]

  const socialMediaLinks = [{
    id: 1,
    icon: linkedInIcon,
    link: "#",
    alt: "ln",
  },
  {
    id: 2,
    icon: facebookIcon,
    link: "#",
    alt: "fb",
  },
  {
    id: 3,
    icon: instagramIcon,
    link: "#",
    alt: "ig",
  },
  {
    id: 4,
    icon: threadsIcon,
    link: "#",
    alt: "t",
  },
  {
    id: 5,
    icon: twitterIcon,
    link: "#",
    alt: "x",
  },]
  return (
    <>
      <h1 className='header'>Get In Touch</h1>
      <div className="body">
      <div className='container'>
        <div className="input-container">
          <h3>Send A Message</h3>
          <form>
            <div className="column">
              <div className="form-inputbox">
                <label for="fname">First Name:</label>
                <input type="text" id="fname" placeholder="Joe" />
              </div>
              <div className="form-inputbox">
                <label for="lname">Last Name:</label>
                <input type="text" id="lname" placeholder="Doe" />
              </div>
            </div>
            <div className="column">
              <div className="form-inputbox">
                <label for="fname">E-mail:</label>
                <input type="text" id="fname" placeholder="joe.doe@panda.com" />
              </div>
              <div className="form-inputbox">
                <label for="fname">Mobile:</label>
                <input type="text" id="fname" placeholder="+916793123456" />
              </div>
            </div>
            <div className="form-MessageBox">
              <label for="fname">Message / Request:</label>
              <textarea id="fname" placeholder="Write Your Message Here" />
            </div>
            <button className="submit-btn">Submit</button>
          </form>
        </div>
        <div className="sub-container">
          <div className="contact-container">
            <h3>Contact Info</h3>
            {contactInfos.map((contactInfo) => (
              <div className="contactInfo" key={contactInfo.id}>
                <img className="contactIcon-bg" src={contactInfo.icon} alt="logos" />
                <span>{contactInfo.text}</span>
              </div>
            ))}
            <div className="socialMedia">
              {socialMediaLinks.map((socialMediaLink) => (
                <span className="socialMedia-style" key={socialMediaLink.id}>
                  <a href={socialMediaLink.link}><img src={socialMediaLink.icon} alt={socialMediaLink.alt} /></a>
                </span>
              ))}
            </div>
          </div>
          <div className="map-container">
            <OpenMap
                  lat={19.021191}
                  lng={72.831064}
                />

          </div>
        </div>
      </div>
      </div>
    </>
  )
}

export default App
